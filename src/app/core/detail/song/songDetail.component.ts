import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Song } from '../../song/song.model';
import { SongService } from '../../song/song.service';

@Component({
  selector: 'app-detail',
  templateUrl: './songDetail.component.html',
})
export class SongDetailComponent implements OnInit {
  songId: string | null = null;
  song: Song | null = null;

  constructor(
    private route: ActivatedRoute,
    private songService: SongService
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.songId = params.get('id');
      this.song = this.songService.getSongById(Number(this.songId));
    });
  }
}
