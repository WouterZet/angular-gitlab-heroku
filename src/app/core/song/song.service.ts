import { Injectable } from '@angular/core';
import { first, Observable, of } from 'rxjs';
import { Song } from './song.model';

@Injectable({
  providedIn: 'root',
})
export class SongService {
  songs: Song[] = [{
      id: 0,
      title: 'Love',
      album: 'Love',
      artists: 'SWIM',
      durationSeconds: 316,
      durationMinutes: "5:16",
    },
    {
      id: 1,
      title: 'Light That Fire',
      album: 'Light That Fire',
      artists: 'Dj Clipps, Bthelick, 9Ts',
      durationSeconds: 266,
      durationMinutes: "4:26",      
    },
    {
      id: 2,
      title: 'I Should Go (feat. Kenny Beats) - Nic Fanciulli Remix',
      album: 'I Should Go (feat. Kenny Beats)[Nic Fanciulli Remix]',
      artists: 'James Vincent McMorrow, Kenny Beats, Nic Fanciulli',
      durationSeconds: 196,
      durationMinutes: "3:16",
    },
    {
      id: 3,
      title: 'The Rain Break',
      album: 'The Rain Break',
      artists: 'Claude VonStroke',
      durationSeconds: 389,
      durationMinutes: "6:29",
    },
    {
      id: 4,
      title: 'Voodoo Babe',
      album: 'Voodoo Babe',
      artists: 'Never Dull',
      durationSeconds: 249,
      durationMinutes: "3:09",
    },
  ];

  constructor() {
    console.log('Service constructor aangeroepen');
  }

  getSongs(): Song[] {
    console.log('getSongs aangeroepen');
    return this.songs;
  }

  getSongsAsObservable(): Observable < Song[] > {
    console.log('getSongsAsObservable aangeroepen');
    return of(this.songs);
  }

  getSongById(id: number): Song {
    console.log('getSongById aangeroepen');
    return this.songs.filter((song) => song.id == id)[0];
  }

  setSongTitle(id: number, title: string) {
    console.log('setSongTitle aangeroepen');
    this.songs.filter((song) => song.id == id)[0].title = title;
  }

  setSongAlbum(id: number, album: string) {
    console.log('setSongAlbum aangeroepen');
    this.songs.filter((song) => song.id == id)[0].album = album;
  }

  setSongArtists(id: number, artists: string) {
    console.log('setSongMaker aangeroepen');
    this.songs.filter((song) => song.id == id)[0].artists = artists;
  }

  setSongDurationSeconds(id: number, durationSeconds: number) {
    console.log('setSongDuration aangeroepen');
    this.songs.filter((song) => song.id == id)[0].durationSeconds = durationSeconds;
  }

  setSongDurationMinutes(id: number, durationMinutes: String) {
    console.log('setSongDuration aangeroepen');
    this.songs.filter((song) => song.id == id)[0].durationMinutes = durationMinutes;
  }

  idChecker() {
    for (var i = 0; i <= this.songs.length; i++) {
      try {
        if (this.songs[i].id != i) {
          return i;
        }
      } catch {
        return i;
      }
    }
    return 0;
  }

  newSong(title: string, artists: string, album: string, durationMinutes: string) {
    console.log('newSong aangeroepen');
    var newSong: Song[] = [{
      id: (this.idChecker()),
      title: title,
      album: album,
      artists: artists,
      durationSeconds: (parseInt(durationMinutes.substring(0, durationMinutes.indexOf(":")), 10)*60) + parseInt(durationMinutes.substring(durationMinutes.indexOf(":")+1), 10),
      durationMinutes: durationMinutes
    }]
    this.songs = this.songs.concat(newSong);
    this.songs.sort(function (a, b) {
      return a.id - b.id;
    });
  }

  deleteSong(num: number) {
    console.log('deleteSong aangeroepen');
    this.songs.forEach((element, index) => {
      if ((this.songs.filter((song) => song.id == num))[0] == element) this.songs.splice(index, 1);
    });
  }
}
