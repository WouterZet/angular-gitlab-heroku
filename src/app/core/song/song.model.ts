export class Song {
  id: number = 0;
  title: string = '';
  album: string = '';
  artists: string = '';
  durationSeconds: number = 0;
  durationMinutes: String = "";
  

  constructor(title = '', album = '', artists = '' ,duration = 0) {
    console.log('constructor in song.model.ts aangeroepen');
    this.title = title;
    this.album = album;
    this.artists = artists;
    this.durationSeconds = duration;
    this.durationMinutes = (Math.floor(duration/60) + ":" + (duration%60))
  }

  
}
