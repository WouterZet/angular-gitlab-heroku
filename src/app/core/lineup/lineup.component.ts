import { Component, OnInit } from '@angular/core';
import { Song } from '../song/song.model';
import { SongService } from '../song/song.service';

@Component({
  selector: 'app-list',
  templateUrl: './lineup.component.html',
  styles: [],
})
export class LineupComponent implements OnInit {
  songs: Song[] = [];

  constructor(private songService: SongService) {}

  ngOnInit(): void {
    this.songs = this.songService.getSongs();
  }
}