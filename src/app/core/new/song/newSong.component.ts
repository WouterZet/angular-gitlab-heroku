import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Song } from '../../song/song.model';
import { SongService } from '../../song/song.service';

@Component({
  selector: 'app-new',
  templateUrl: './newSong.component.html',
})
export class NewSongComponent implements OnInit {
  songId: string | null = null;
  song: Song | null = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private songService: SongService
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
        console.log('ngOnInit in newSong aangeroepen'); 
      }
    );
  }

  save() {
    console.log('save aangeroepen');
    if ((<HTMLInputElement>document.getElementById("titleInput")).value != "" && ((<HTMLInputElement>document.getElementById("artistsInput")).value) != "" && ((<HTMLInputElement>document.getElementById("albumInput")).value) != "" && ((<HTMLInputElement>document.getElementById("durationInput")).value) != "") {
      console.log('savecheck bevestigd');
      this.songService.newSong(((<HTMLInputElement>document.getElementById("titleInput")).value), ((<HTMLInputElement>document.getElementById("artistsInput")).value), ((<HTMLInputElement>document.getElementById("albumInput")).value), ((<HTMLInputElement>document.getElementById("durationInput")).value));
      this.router.navigate(['..'], { relativeTo: this.route });
    } else {
      alert("Make sure to fill everything in correctly")
    }
  }
}
