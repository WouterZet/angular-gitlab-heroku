import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User, UserRole } from '../../user/user.model';
import { UserService } from '../../user/user.service';

@Component({
  selector: 'app-new',
  templateUrl: './newUser.component.html',
})
export class NewUserComponent implements OnInit {
  userId: string | null = null;
  user: User | null = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
        console.log('ngOnInit in newUser aangeroepen'); 
      }
    );
  }

  save() {
    console.log('save aangeroepen');
    if ((<HTMLInputElement>document.getElementById("firstNameInput")).value != "" && ((<HTMLInputElement>document.getElementById("lastNameInput")).value) != "" && ((<HTMLInputElement>document.getElementById("emailInput")).value) != "") {
      console.log('savecheck bevestigd');
      this.userService.newUser(((<HTMLInputElement>document.getElementById("firstNameInput")).value), ((<HTMLInputElement>document.getElementById("lastNameInput")).value), ((<HTMLInputElement>document.getElementById("emailInput")).value), UserRole.guest);
      this.router.navigate(['..'], { relativeTo: this.route });
    } else {
      alert("Make sure to fill everything in correctly")
    }
  }
}
