import { TestBed } from '@angular/core/testing';
import { UserRole } from './user.model';
import { UserService } from './user.service';

describe('UserService', () => {
  let service: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserService);
  });

  //test if service is created
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  //test Create
  it('should create a user', () => {
    service.newUser("NewUserFirstName", "NewUserLastName", "NewUser@EmailAdress.com", UserRole.guest);
    expect(service.getUserById(5).firstName).toBe("NewUserFirstName");
    expect(service.getUserById(5).lastName).toBe("NewUserLastName");
    expect(service.getUserById(5).emailAdress).toBe("NewUser@EmailAdress.com");
  });

  //test Read
  it('should read a user', () => {
    service.newUser("NewUserFirstName", "NewUserLastName", "NewUser@EmailAdress.com", UserRole.guest);
    expect(service.getUserById(5).firstName).toBe("NewUserFirstName");
    expect(service.getUserById(5).lastName).toBe("NewUserLastName");
    expect(service.getUserById(5).emailAdress).toBe("NewUser@EmailAdress.com");
  });

  //test Update
  it('should update a user', () => {
    service.newUser("NewUserFirstName", "NewUserLastName", "NewUser@EmailAdress.com", UserRole.guest);
    service.setUserFirstName(5, "NewUserFirstNameUpdated");
    service.setUserLastName(5, "NewUserLastNameUpdated");
    service.setUserEmail(5, "NewUser@UpdatedEmailAdress.com");
    expect(service.getUserById(5).firstName).toBe("NewUserFirstNameUpdated");
    expect(service.getUserById(5).lastName).toBe("NewUserLastNameUpdated");
    expect(service.getUserById(5).emailAdress).toBe("NewUser@UpdatedEmailAdress.com");
  });

  //test Delete
  it('should delete a user', () => {
    service.newUser("NewUserFirstName", "NewUserLastName", "NewUser@EmailAdress.com", UserRole.guest);
    expect(service.getUserById(5).firstName).toBe("NewUserFirstName");
    service.deleteUser(5);
    expect(service.getUserById(5)).toBeNull;
  });
});
