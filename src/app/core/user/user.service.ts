import { Injectable } from '@angular/core';
import { first, Observable, of } from 'rxjs';
import { servicesVersion } from 'typescript';
import { User, UserRole } from './user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  users: User[] = [{
      id: 0,
      firstName: 'Thijme',
      lastName: 'Joosten',
      emailAdress: 't.joosten@gmail.com',
      role: UserRole.admin,
    },
    {
      id: 1,
      firstName: 'Jan',
      lastName: 'van Muijden',
      emailAdress: 'muijdenjan@gmail.com',
      role: UserRole.guest,
    },
    {
      id: 2,
      firstName: 'Tom',
      lastName: 'de Jager',
      emailAdress: 'huntertom@gmail.com',
      role: UserRole.editor,
    },
    {
      id: 3,
      firstName: 'Ralf',
      lastName: 'Soeren',
      emailAdress: 'ralf23565@gmail.com',
      role: UserRole.guest,
    },
    {
      id: 4,
      firstName: 'Marijn',
      lastName: 'Degeling',
      emailAdress: 'marijnvvt@gmail.com',
      role: UserRole.guest,
    },
  ];

  constructor() {
    console.log('Service constructor aangeroepen');
  }

  getUsers(): User[] {
    console.log('getUsers aangeroepen');
    console.log("Answer: " + this.getUserById(5));
    console.log("what?");
    return this.users;
  }

  getUsersAsObservable(): Observable < User[] > {
    console.log('getUsersAsObservable aangeroepen');
    // 'of' is een rxjs operator die een Observable
    // maakt van de gegeven data.
    return of(this.users);
  }

  getUserById(id: number): User {
    console.log('getUserById aangeroepen');
    return this.users.filter((user) => user.id == id)[0];
  }

  setUserFirstName(id: number, firstName: string) {
    console.log('setUserFirstName aangeroepen');
    this.users.filter((user) => user.id == id)[0].firstName = firstName;
  }

  setUserLastName(id: number, lastName: string) {
    console.log('setUserLastName aangeroepen');
    this.users.filter((user) => user.id == id)[0].lastName = lastName;
  }

  setUserEmail(id: number, emailAdress: string) {
    console.log('setUserEmail aangeroepen');
    this.users.filter((user) => user.id == id)[0].emailAdress = emailAdress;
  }

  idChecker() {
    for (var i = 0; i <= this.users.length; i++) {
      try {
        if (this.users[i].id != i) {
          return i;
        }
      } catch {
        return i;
      }
    }
    return 0;
  }

  newUser(firstName: string, lastName: string, emailAdress: string, role: UserRole) {
    console.log('newUser aangeroepen');
    var newUser: User[] = [{
      id: (this.idChecker()),
      firstName: firstName,
      lastName: lastName,
      emailAdress: emailAdress,
      role: role,
    }]
    this.users = this.users.concat(newUser);
    this.users.sort(function (a, b) {
      return a.id - b.id;
    });
  }

  deleteUser(num: number) {
    console.log('deleteUser aangeroepen');
    this.users.forEach((element, index) => {
      if ((this.users.filter((user) => user.id == num))[0] == element) this.users.splice(index, 1);
    });
  }
}
