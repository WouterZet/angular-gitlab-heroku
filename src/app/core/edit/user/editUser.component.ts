import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../user/user.model';
import { UserService } from '../../user/user.service';

@Component({
  selector: 'app-edit',
  templateUrl: './editUser.component.html',
})
export class EditUserComponent implements OnInit {
  userId: string | null = null;
  user: User | null = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.userId = params.get('id');
      if (this.userId) {
        this.user = this.userService.getUserById(Number(this.userId));
      } else {
        console.log('else in ngOnInit in editUser aangeroepen');
        this.user = new User();
      }
    });
  }

  delete() {
    console.log('delete aangeroepen');
    var answer = window.confirm("Are you sure you want to delete user?");
    if (answer) {
      console.log('delete bevestigd');
      this.userService.deleteUser(Number(this.userId));
      this.router.navigate(['/userlist/'], {
        relativeTo: this.route
      });
    } else {
      console.log('delete afgebroken');
    }
  }

  save() {
    console.log('save aangeroepen');
    if (( < HTMLInputElement > document.getElementById("firstNameInput")).value != "") {
      this.userService.setUserFirstName(Number(this.userId), ( < HTMLInputElement > document.getElementById("firstNameInput")).value);
      if (( < HTMLInputElement > document.getElementById("lastNameInput")).value != "") {
        this.userService.setUserLastName(Number(this.userId), ( < HTMLInputElement > document.getElementById("lastNameInput")).value);
        if (( < HTMLInputElement > document.getElementById("emailInput")).value != "") {
          this.userService.setUserEmail(Number(this.userId), ( < HTMLInputElement > document.getElementById("emailInput")).value);
          this.router.navigate(['/userlist/'], {
            relativeTo: this.route
          });
        } else {
          alert("Emailadress was not filled in correctly")
        }
      } else {
        alert("Last name was not filled in correctly")
      }
    } else {
      alert("First name was not filled in correctly")
    }
  }
}