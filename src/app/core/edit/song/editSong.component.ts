import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Song } from '../../song/song.model';
import { SongService } from '../../song/song.service';

@Component({
  selector: 'app-edit',
  templateUrl: './editSong.component.html',
})
export class EditSongComponent implements OnInit {
  songId: string | null = null;
  song: Song | null = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private songService: SongService
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.songId = params.get('id');
      if (this.songId) {
        this.song = this.songService.getSongById(Number(this.songId));
      } else {
        console.log('else in ngOnInit in editSong aangeroepen');
        this.song = new Song();
      }
    });
  }

  delete() {
    console.log('delete aangeroepen');
    var answer = window.confirm("Are you sure you want to delete song?");
    if (answer) {
      console.log('delete bevestigd');
      this.songService.deleteSong(Number(this.songId));
      this.router.navigate(['/lineup/'], {
        relativeTo: this.route
      });
    } else {
      console.log('delete afgebroken');
    }
  }

  save() {
    console.log('save aangeroepen');
    if (( < HTMLInputElement > document.getElementById("titleInput")).value != "") {
      this.songService.setSongTitle(Number(this.songId), ( < HTMLInputElement > document.getElementById("titleInput")).value);
      if (( < HTMLInputElement > document.getElementById("artistsInput")).value != "") {
        this.songService.setSongArtists(Number(this.songId), ( < HTMLInputElement > document.getElementById("artistsInput")).value);
        if (( < HTMLInputElement > document.getElementById("albumInput")).value != "") {
          this.songService.setSongAlbum(Number(this.songId), ( < HTMLInputElement > document.getElementById("albumInput")).value);
          if (( < HTMLInputElement > document.getElementById("durationInput")).value != "") {
            this.songService.setSongDurationMinutes(Number(this.songId), ( < HTMLInputElement > document.getElementById("durationInput")).value);
            this.router.navigate(['/lineup/'], {
              relativeTo: this.route
            });
          } else {
            alert("Duration was not filled in correctly")
          }
        } else {
          alert("Album was not filled in correctly")
        }
      } else {
        alert("Artists were not filled in correctly")
      }
    } else {
      alert("Title was not filled in correctly")
    }
  }
}