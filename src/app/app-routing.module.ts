import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { UserListComponent } from './core/userlist/userlist.component'
import { LineupComponent } from './core/lineup/lineup.component'
import { LayoutComponent } from './core/layout/layout.component'
import { UsecasesComponent } from './pages/about/usecases/usecases.component'
import { EditUserComponent } from './core/edit/user/editUser.component'
import { EditSongComponent } from './core/edit/song/editSong.component'
import { UserDetailComponent } from './core/detail/user/userDetail.component'
import { SongDetailComponent } from './core/detail/song/songDetail.component'
import { NewUserComponent } from './core/new/user/newUser.component'
import { NewSongComponent } from './core/new/song/newSong.component'

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'userlist' },
      { path: 'lineup', component: LineupComponent },
      { path: 'lineup/new', pathMatch: 'full', component: NewSongComponent },
      { path: 'lineup/:id', pathMatch: 'full', component: SongDetailComponent },
      { path: 'lineup/:id/edit', pathMatch: 'full', component: EditSongComponent },
      { path: 'userlist', component: UserListComponent },
      { path: 'userlist/new', pathMatch: 'full', component: NewUserComponent },
      { path: 'userlist/:id', pathMatch: 'full', component: UserDetailComponent },
      { path: 'userlist/:id/edit', pathMatch: 'full', component: EditUserComponent },
      { path: 'about', component: UsecasesComponent }
    ]
  },
  { path: '**', redirectTo: '/' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
