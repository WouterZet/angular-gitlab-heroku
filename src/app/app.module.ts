import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { RouterModule } from '@angular/router'
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { UserListComponent } from './core/userlist/userlist.component'
import { LineupComponent } from './core/lineup/lineup.component'
import { UserDetailComponent } from './core/detail/user/userDetail.component'
import { SongDetailComponent } from './core/detail/song/songDetail.component'
import { NewUserComponent } from './core/new/user/newUser.component'
import { NewSongComponent } from './core/new/song/newSong.component'
import { EditUserComponent } from './core/edit/user/editUser.component'
import { EditSongComponent } from './core/edit/song/editSong.component'
import { NavbarComponent } from './core/navbar/navbar.component'
import { UsecasesComponent } from './pages/about/usecases/usecases.component'
import { UsecaseComponent } from './pages/about/usecases/usecase/usecase.component'
import { LayoutComponent } from './core/layout/layout.component';
import { FooterComponent } from './core/footer/footer.component'

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LayoutComponent,
    UsecasesComponent,
    UsecaseComponent,
    UserListComponent,
    LineupComponent,
    FooterComponent,
    UserDetailComponent,
    SongDetailComponent,
    NewUserComponent,
    NewSongComponent,
    EditUserComponent,
    EditSongComponent
  ],
  imports: [BrowserModule, RouterModule, FormsModule, NgbModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
