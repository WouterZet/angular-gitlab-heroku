import {
  Component,
  OnInit
} from '@angular/core'
import {
  UseCase
} from '../usecase.model'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html'
})
export class UsecasesComponent implements OnInit {
  readonly PLAIN_USER = 'Reguliere gebruiker'
  readonly ADMIN_USER = 'Administrator'

  useCases: UseCase[] = [{
      id: 'UC-01',
      name: 'Editing user or song',
      description: 'This allows the data of a user or song to be changed.',
      scenario: [
        'User chooses the person or song to change.',
        'User chooses to edit the data',
        'User fills in the custom data',
        'System changes the data'
      ],
      actor: this.ADMIN_USER,
      precondition: 'None',
      postcondition: 'The data of the user or song has been changed'
    },
    {
      id: 'UC-02',
      name: 'Adding user or song',
      description: 'This allows the data of a user or song to be added.',
      scenario: [
        'User chooses to add a new entry', 
        'User fills in the custom data', 
        'System adds the new entry'
    ],
      actor: this.ADMIN_USER,
      precondition: 'The actor is logged in',
      postcondition: 'A new entry has been added'
    }
  ]

  constructor() {}

  ngOnInit() {}
}
